import pycparser
from pycparser.c_ast import BinaryOp, ArrayDecl, Typedef, Struct, TypeDecl, PtrDecl, ArrayRef, Assignment


class C2PythonTranslator:
    def __init__(self):
        self.typedefs = {}
        self.indent_level = 0
        self.indent_count = 4
        self.indent_character = ' '
        self.nodes_stack = []
        self.structs_types = {}
        self.structs_variables = {}

        self.pointer_already_dereferenced = False
        self.pointer_variables = {}
        self.variables_convert_to_pointers = []
        self.variables_converted_to_pointers = []

    def parse(self, c_file_path):
        code_lines = []

        for node in self._get_nodes(c_file_path):
            self.pointer_already_dereferenced = False
            self.pointer_variables = {}
            self.variables_convert_to_pointers = []
            self.variables_converted_to_pointers = []
            self._parse_node(node)

            code_lines.append(self._parse_node(node))

        PY_START_MAIN = 'if __name__ == "__main__":\n' + \
                        '{}main()'.format(self.indent_character * self.indent_count)

        return ''.join((i for i in code_lines if i is not None)) + \
            PY_START_MAIN

    def _get_nodes(self, c_file_path):
        return pycparser.parse_file(
            filename=c_file_path,
            use_cpp='True',
            cpp_args='-I./pycparser-github/utils/fake_libc_include'
        ).ext

    def _parse_node(self, node):
        self.nodes_stack.append(node)
        parsed_node = getattr(self, '_parse_{}'.format(node.__class__.__name__))(node)
        self.nodes_stack.pop()
        return parsed_node

    def _enter(self):
        self.indent_level += 1

    def _exit(self):
        self.indent_level -= 1

    def _parse_Typedef(self, node):
        """
        Parses Typedefs.
        It is made just in case since we rather don't need them.
        """
        alias_type_name, real_type = node.type.declname, node.type.type.names[0]
        if alias_type_name not in self.typedefs:
            self.typedefs[alias_type_name] = real_type

        # the same typedef with different type?!? should never happen!
        elif self.typedefs[alias_type_name] != real_type:
            print("Multiple typedef = " + alias_type_name + " = " + real_type)
            print("Old typedef = {}".format(self.typedefs[alias_type_name]))

    def _parse_FuncDef(self, function_node):
        """
        Translates C function definition into Python code.
        """
        func_name = function_node.decl.name

        if function_node.decl.type.args:
            func_args_list = (i.name for i in function_node.decl.type.args.params)
        else:
            func_args_list = []

        code = 'def {}({}):\n'.format(func_name, ', '.join(func_args_list))

        self._enter()
        code += '\n'.join(
            (self._indent_code(self._parse_node(node)) for node in function_node.body.block_items)
        )
        self._exit()
        code += '\n\n'

        return code

    def _parse_FuncCall(self, func_call_node):
        """
        Translates given C function call node into corresponding Python code.

        Contains mapping for special cases like `printf` function.
        """
        c2py_func_mapping = {
            'printf': self.__translate_printf,
            'puts': self.__translate_puts,
            'malloc': self.__translate_malloc
        }

        py_func_name = func_call_node.name.name

        if func_call_node.args:
            py_args_tuple = tuple((self._parse_node(i) for i in func_call_node.args.exprs))
        else:
            py_args_tuple = tuple()

        if py_func_name in c2py_func_mapping:
            return c2py_func_mapping[py_func_name](py_args_tuple)

        py_args = ', '.join(py_args_tuple)
        return "{}({})".format(py_func_name, py_args)

    def __translate_printf(self, py_args_tuple):
        """
        Translates C `printf` function into Python `print` function.

        If there is more than one argument, arguments are parsed into Python '%' operator.
        """
        format_str, *args = py_args_tuple

        if args:
            print_args = '{} % ({},), end=""'.format(format_str, ', '.join(args))
        else:
            print_args = format_str + ', end=""'

        return "print({})".format(print_args)

    def __translate_puts(self, py_args_tuple):
        arg = py_args_tuple[0]

        # if the argument is not a real C string, but a pointer (const char*) then it needs to be 'dereferenced'
        if not (arg.startswith('"') and arg.endswith('"')):
            arg += "['value']"

        return "print({})".format(arg)

    def __translate_malloc(self, py_args_tuple):
        code = "[None] * ({})".format(py_args_tuple[0])

        # when malloc is used as RHS of assignment - means its a pointer assignment
        if isinstance(self.nodes_stack[-2], Assignment):
            code = self._create_ref(code)

        return code

    def _parse_Assignment(self, assignment_node):
        return "{} {} {}".format(
            self._parse_node(assignment_node.lvalue),
            assignment_node.op,
            self._parse_node(assignment_node.rvalue)
        )

    def _parse_Constant(self, constant_node):
        """
        Translates Constant node into Python code.
        Examples of Constant nodes:
            3
            4.5
            -12312
            "saasd"
            's'

        But:
            3.5f    is changed to   3.5
        """
        if constant_node.type == 'float' and constant_node.value[-1].lower() == 'f':
            return constant_node.value[:-1]

        return constant_node.value

    def _parse_BinaryOp(self, binaryop_node):
        """
        Translates BinaryOp node into Python code.
        Examples of BinaryOp nodes:
            3+2
            3 + 2 * x
            (15+2.2)/3
        """
        operator_mappings = {
            '&&': 'and',
            '||': 'or'
        }

        get_node_py_str = lambda n: ('({})' if isinstance(n, BinaryOp) else '{}').format(self._parse_node(n))
        op = operator_mappings.get(binaryop_node.op, binaryop_node.op)
        return "{} {} {}".format(get_node_py_str(binaryop_node.left), op, get_node_py_str(binaryop_node.right))

    def _parse_ID(self, id_node):
        """
        Translated ID node into Python code.
        Examples of ID nodes:
            x
            foo
            bar
        """

        name = id_node.name

        if name in self.variables_converted_to_pointers and not self.pointer_already_dereferenced:
            return "{}['value']".format(name)

        return name

    def _parse_UnaryOp(self, unaryop_node):
        """
        CURRENTLY NOT SUPPORTED - FALLBACKS TO THE EXPRESSION NODE IT CONTAINS.
        Examples of UnaryOp nodes:
            &x
            *x
        """
        unaryop_mapping = {
            'p--': '{} -= 1',
            '--p': '{} -= 1',
            'p++': '{} += 1',
            '++p': '{} += 1',

            '++': '{} += 1',
            '--': '{} -= 1',
            '*': "{}['value']"
        }
        if unaryop_node.op == '&':
            # actually here the pointer was not dereferenced,
            # but taking the address means we don't really want it to be treated like a pointer
            # ( mostly it means we are in such line: int* ptr = &a; )
            self.pointer_already_dereferenced = True
            return self._parse_node(unaryop_node.expr)

        if unaryop_node.op in unaryop_mapping:
            return unaryop_mapping[unaryop_node.op].format(self._parse_node(unaryop_node.expr))

        if unaryop_node.op == 'sizeof':
            return '1'

        raise NotImplementedError("UnaryOp: {} not implemented.".format(unaryop_node.op))

    def _parse_ArrayRef(self, arrayref_node):
        ## getting array name without subscripts ([] etc)
        # this means we are in deeper ArrayRef like a[i][j] - in the 'j' parsing, we need to get just 'a' name
        name_node = arrayref_node.name
        while isinstance(name_node, ArrayRef):
            name_node = name_node.name

        array_name = self._parse_node(name_node)
        array_subscription = self._parse_node(arrayref_node.name)

        array_index = self._parse_node(arrayref_node.subscript)

        if array_name in self.pointer_variables:
            self.pointer_already_dereferenced = True
            array_name = "{}['value']".format(array_subscription)

        return "{}[{}]".format(array_name, array_index)

    def _parse_Decl(self, decl_node):
        """
        Translates Decl node into Python code.

        Examples:
            int x;      ==> 'x = None'
            int x = 5;  ==> 'x = 5'
            int b[3];  ==> b = [0, 0, 0]

            struct imaginary
            {
                int real;
                int complex;
            };
            ==> converts imaginary instances into dicts
        """
        # lhs - name of variable
        lhs = decl_node.name

        if isinstance(decl_node.type, Struct):
            self._parse_node(decl_node.type)
            return ''

        is_ptr = isinstance(decl_node.type, PtrDecl)
        if is_ptr:
            ptr_decl = decl_node.type
            ptr_level = 1
            while isinstance(ptr_decl.type, PtrDecl):
                ptr_level += 1
                ptr_decl = ptr_decl.type

            self.pointer_variables[lhs] = ptr_level

        rhs = self._parse_node(decl_node.init) if decl_node.init else 'None'
        rhs_is_ref_assignment = rhs in self.pointer_variables or self.pointer_already_dereferenced

        if self.pointer_already_dereferenced and rhs not in self.pointer_variables:
            self.variables_convert_to_pointers.append(rhs)

        self.pointer_already_dereferenced = False

        if lhs in self.variables_convert_to_pointers:
            self.variables_convert_to_pointers.remove(lhs)
            self.variables_converted_to_pointers.append(lhs)
            ptr_level = 1
            is_ptr = True

        # if declaration is a pointer, it has to be wrapped to act as a reference type in Python
        if not rhs_is_ref_assignment and is_ptr:
            while ptr_level != 0:
                rhs = self._create_ref(rhs)
                ptr_level -= 1

        # if rhs wasn't an InitList (like {1, 2,3}), check whether we are declaring an array...
        # if so, declare an array of None items of specified size
        if rhs == 'None' and isinstance(decl_node.type, ArrayDecl):
            array_size = int(decl_node.type.dim.value)
            rhs = '[None] * {}'.format(array_size)

        return "{} = {}".format(lhs, rhs)

    def _parse_Struct(self, struct_node):
        """
        Parses Struct node. Actually this is not translated because struct node is just a struct declaration.

        Example:
            struct x { int a; int b; };
        """
        struct_data = {}    # keys = field names, values = field types

        for field_decl in struct_node.decls:
            struct_data[field_decl.name] = field_decl.type.type.names[0]

        self.structs_types[struct_node.name] = struct_data

    def _parse_StructRef(self, structref_node):
        return '{}["{}"]'.format(structref_node.name.name, structref_node.field.name)

    def _parse_If(self, if_node):
        # first If line is indented in _parse_FuncDef
        code = 'if {}:\n'.format(self._parse_node(if_node.cond))

        self._enter()
        code += self._indent_code(self._parse_node(if_node.iftrue))
        self._exit()

        if if_node.iffalse:
            code += '\n'
            code += self._indent_code('else:\n')
            self._enter()

            code += self._indent_code(self._parse_node(if_node.iffalse))

            self._exit()

        return code

    def _parse_For(self, for_node):
        """
        Translates For into Python while loop.
        It is because translating complex for loops from C can't be implemented as Python for loops.
        """
        loop_cond = self._parse_node(for_node.cond)

        if for_node.init:
            loop_init = self._parse_node(for_node.init)
            code = '{}\n'.format(loop_init)
            code += self._indent_code("while {}:\n".format(loop_cond))
        else:
            code = "while {}:\n".format(loop_cond)

        loop_step = self._parse_node(for_node.next)

        self._enter()

        code += self._indent_code(self._parse_node(for_node.stmt)) + '\n'
        code += self._indent_code(loop_step)

        self._exit()

        return code

    def _parse_While(self, while_node):
        code = 'while {}:\n'.format(self._parse_node(while_node.cond))

        self._enter()
        code += self._indent_code(self._parse_node(while_node.stmt))
        self._exit()

        return code

    def _parse_Break(self, break_node):
        return "break"

    def _parse_Continue(self, continue_node):
        return "continue"

    def _parse_Compound(self, compound_node):
        """
        The first node is not indented because Compound is used in `_parse_If` which indents the parsed too...
        """
        first, *rest = compound_node.block_items
        code = self._parse_node(first)

        if rest:
            code += '\n'
            code += '\n'.join(
                (self._indent_code(self._parse_node(node)) for node in rest)
            )

        return code

    def _parse_InitList(self, initlist_node):
        decl_node = self.nodes_stack[-2]
        if isinstance(decl_node.type, ArrayDecl):
            if decl_node.type.dim:
                array = ['0'] * int(decl_node.type.dim.value)
                for index, item in enumerate(initlist_node.exprs):
                    array[index] = str(self._parse_node(item))

            else:
                array = (self._parse_node(n) for n in initlist_node.exprs)
        elif isinstance(decl_node.type, TypeDecl):
            struct_type = decl_node.type.type.name
            variable_name = decl_node.name
            self.structs_variables[variable_name] = struct_type
            kv_pairs = zip(self.structs_types[struct_type].keys(), (self._parse_node(n) for n in initlist_node.exprs))
            dict_content = ', '.join(('"{k}": {v}'.format(k=k, v=v) for k, v in kv_pairs))
            return "{" + dict_content + "}"

        else:
            raise NotImplementedError("Currently InitList implements only ArrayDecl and TypeDecl cases")

        return "[{}]".format(', '.join(array))

    def _parse_Return(self, return_node):
        """
        Translates given C return node into corresponding Python code.
        """
        return "return {}".format(self._parse_node(return_node.expr))

    def _indent_code(self, line):
        return "{}{}".format(self.indent_level * self.indent_count * self.indent_character, line)

    def _create_ref(self, rhs):
        return "{'value': " + rhs + "}"


with open('test.py', 'w') as fp:
    fp.write(C2PythonTranslator().parse('test.c'))
