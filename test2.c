#include <stdio.h>

typedef double real;
struct imag {
    real real;
    real complex;
};

int main() {
    struct imag x = {5, 4};
    printf("Imag real=%f, complex=%f\n", x.real, x.complex);

    return 0;
}
