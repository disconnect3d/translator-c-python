#include <stdio.h>
#include <stdlib.h>


void pointers()
{
    int a;
    int* x = malloc(sizeof(int) * 2);
    *x = 5;

    int* y = &a;
    int* z = y;
    a = 2;
    *y = 123;

    printf("x = %d\n", *x);
    printf("a = %d\n", a);
    printf("y = %d\n", *y);
    printf("z = %d\n", *z);
}

typedef double real_t;
struct imag {
    real_t real;
    real_t complex;
};

void struct_test()
{
    struct imag x = {5, 4};
    x.real = 12;
    x.complex = 32;

    printf("Imag real=%f, complex=%f\n", x.real, x.complex);
}

#define MACRO() puts("This printf is sponsored by #define")

void malloc_test()
{
    MACRO();
    int arr_size = 5;
    int **a = malloc(sizeof(int*) * arr_size);
    int i;
    int j;

    for(i=0; i<arr_size; ++i)
    {
        a[i] = malloc(sizeof(int) * arr_size);
        for(j=0; j<arr_size; ++j)
            a[i][j] = 10 * i + j;
    }

    for(i=0; i<arr_size; ++i)
        for(j=0; j<arr_size; ++j)
            printf("a[i][j] = %d\n", a[i][j]);
}

void strings()
{
    const char* x = "Hello World!";
    puts(x);
}

void arrays()
{
    int a[] = {1, 2, 3};
    int c[10] = {1, 2};

    int b[10];

    int i=0;
    for(; i<10; ++i)
        b[i] = i;
}

float while_break_continue(float x)
{
    while (1)
    {
        if (x > 5.0f || x < 2.0f)
            break;

        else if (x < 4.0f)
        {
            x -= 1.2f;
            continue;
        }
        x -= 5;
    }

    return x;
}

void fizzbuz(int max_number)
{
    int i;
    for(i=1; i<=max_number; ++i)
    {
        if (i % 3 == 0)
            printf("Fizz");
        if (i % 5 == 0)
            printf("Buzz");
        if ((i % 3 != 0) && (i % 5 != 0))
            printf("number=%d", i);
        printf("\n");
    }
}

int add(int a, int b)
{
    return a + b * b - a / (a + b);
}

double foobar(int x)
{
    if (x < 1)
        x--;

    if (x > 2)
    {
        x += 2;
        return x;
    }
    else
        return 1.5;
}

int main()
{
    int a;
    int x = 6;
    double y = 5.5 * 123 - 123 / 5 + 2 - 512 * (123 * 55.2);
    double z = x + y * add(3 + 4, 7);

    int *ptr = &a;

    macro_test();
    strings();
    arrays();
    printf("while_break_cont(3.1f) = %f\n", while_break_continue(3.1f));
    printf("while_break_cont(1) = %f\n", while_break_continue(1));
    printf("while_break_cont(4.2) = %f\n", while_break_continue(4.2));
    fizzbuz(30);
    printf("foobar(2) = %d\n", foobar(2));
    printf("foobar(0) = %d\n", foobar(0));
    printf("foobar(5) = %d\n", foobar(5));
    pointers();
    return 0;
}