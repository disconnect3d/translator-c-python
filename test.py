def pointers():
    a = {'value': None}
    x = {'value': [None] * (1 * 2)}
    x['value'] = 5
    y = a
    z = y
    a['value'] = 2
    y['value'] = 123
    print("x = %d\n" % (x['value'],), end="")
    print("a = %d\n" % (a['value'],), end="")
    print("y = %d\n" % (y['value'],), end="")
    print("z = %d\n" % (z['value'],), end="")

def struct_test():
    x = {"real": 5, "complex": 4}
    x["real"] = 12
    x["complex"] = 32
    print("Imag real=%f, complex=%f\n" % (x["real"], x["complex"],), end="")

def malloc_test():
    print("This printf is sponsored by #define")
    arr_size = 5
    a = {'value': {'value': [None] * (1 * arr_size)}}
    i = None
    j = None
    i = 0
    while i < arr_size:
        a['value'][i] = {'value': [None] * (1 * arr_size)}
        j = 0
        while j < arr_size:
            a['value'][i]['value'][j] = (10 * i) + j
            j += 1
        i += 1
    i = 0
    while i < arr_size:
        j = 0
        while j < arr_size:
            print("a[i][j] = %d\n" % (a['value'][i]['value'][j],), end="")
            j += 1
        i += 1

def strings():
    x = {'value': "Hello World!"}
    print(x['value'])

def arrays():
    a = [1, 2, 3]
    c = [1, 2, 0, 0, 0, 0, 0, 0, 0, 0]
    b = [None] * 10
    i = 0
    while i < 10:
        b[i] = i
        i += 1

def while_break_continue(x):
    while 1:
        if (x > 5.0) or (x < 2.0):
            break
        else:
            if x < 4.0:
                x -= 1.2
                continue
        x -= 5
    return x

def fizzbuz(max_number):
    i = None
    i = 1
    while i <= max_number:
        if (i % 3) == 0:
            print("Fizz", end="")
        if (i % 5) == 0:
            print("Buzz", end="")
        if ((i % 3) != 0) and ((i % 5) != 0):
            print("number=%d" % (i,), end="")
        print("\n", end="")
        i += 1

def add(a, b):
    return (a + (b * b)) - (a / (a + b))

def foobar(x):
    if x < 1:
        x -= 1
    if x > 2:
        x += 2
        return x
    else:
        return 1.5

def main():
    a = {'value': None}
    x = 6
    y = (((5.5 * 123) - (123 / 5)) + 2) - (512 * (123 * 55.2))
    z = x + (y * add(3 + 4, 7))
    ptr = a
    macro_test()
    strings()
    arrays()
    print("while_break_cont(3.1f) = %f\n" % (while_break_continue(3.1),), end="")
    print("while_break_cont(1) = %f\n" % (while_break_continue(1),), end="")
    print("while_break_cont(4.2) = %f\n" % (while_break_continue(4.2),), end="")
    fizzbuz(30)
    print("foobar(2) = %d\n" % (foobar(2),), end="")
    print("foobar(0) = %d\n" % (foobar(0),), end="")
    print("foobar(5) = %d\n" % (foobar(5),), end="")
    pointers()
    return 0

if __name__ == "__main__":
    main()